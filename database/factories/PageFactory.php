<?php

namespace Database\Factories;

use App\Models\Page;
use Illuminate\Database\Eloquent\Factories\Factory;

class PageFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Page::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            "title" => $this->faker->text(50),
            "slug" => $this->faker->text(25),
            "meta_keywords" => $this->faker->text(5),
            "meta_description" => $this->faker->text(140),
            "short_text" => $this->faker->text(),
            "text" => $this->faker->text(),
            "views" => rand(0, 200),
            "active" => rand(0,1),
        ];
    }
}
