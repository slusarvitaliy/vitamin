<?php

use App\Http\Controllers\Dashboard\{
    PageController,
    UserController
};
use Illuminate\Support\Facades\Route;

Route::middleware("auth")->group(function () {
    Route::get("", function () {
        return view("dashboard.index");
    });
    Route::get('/users', [UserController::class, "index"]);
    Route::post('/users/add', [UserController::class, "add"])->name("add-user");
    Route::post('/users/update', [UserController::class, "update"])->name("update-user");
    Route::get('/users/remove/{id}', [UserController::class, "remove"]);

    Route::get('/pages', [PageController::class, "index"])->name('adm-pages');
    Route::get('/pages/filter', [PageController::class, "filter"]);
    Route::get('/pages/edit/{id}', [PageController::class, "edit"]);
    Route::get('/pages/activate/{id}/{value}', [PageController::class, "activate"]);
    Route::post('/pages/update', [PageController::class, "update"])->name("update-page");

    Route::get('/pages/add', function() {
        return view("dashboard.pages.add-page");
    })->name("add-page");

    Route::post('/pages/add', [PageController::class, "add"])->name("add-page");
    Route::get('/pages/remove/{id}/{url?}', [PageController::class, "remove"]);
    Route::get('/pages/duplicate/{id}', [PageController::class, "duplicate"]);
});
