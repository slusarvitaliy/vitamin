<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Page;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Str;

class PageController extends Controller
{
    public function index(Request $request)
    {
        $pages = Page::orderBy('id', 'desc')->paginate(10);
        $filtered = 0;
        $search = '';

        return view("dashboard.pages.pages", compact("pages", "filtered", "search"));
    }

    public function filter(Request $request)
    {
//        $request->dd();
        $pages = [];
        $search = old("search", "");

        if(!empty($request->input("id_sort"))) {
            $pages = Page::orderBy('id', $request->input("id_sort"));
        } elseif (!empty($request->input("name_sort"))) {
            $pages = Page::orderBy('title', $request->input("name_sort"));
        } elseif (!empty($request->input("active_sort"))) {
            $pages = Page::orderBy('active', $request->input("active_sort"));
        } elseif (!empty($request->input("created_sort"))) {
            $pages = Page::orderBy('created_at', $request->input("created_sort"));
        } else {
            $pages = Page::orderBy('id', 'desc');
        }

        if(!empty($request->input("search"))) {
            $search = $request->input("search");
            $pages->where("title", "LIKE", "%". $search ."%");
        }

        if(!empty($pages)) {
            $pages = $pages->paginate(30);
        }

        $filtered = 1;

        return view("dashboard.pages.pages", compact("pages", "filtered", "search"));
    }

    public function add(Request $request)
    {
        Page::create([
            "title" => $request->input("title"),
//            "slug" => Str::slug($request->input("title"), '-'),
            "meta_keywords" => $request->input("meta-keywords"),
            "meta_description" => $request->input("meta-description"),
            "short_text" => $request->input("short-text"),
            "text" => $request->input("text"),
            "views" => 1,
            "active" => $request->input("active"),
        ]);

        if(!empty($request->input("create_new"))) {
            return redirect()->route("add-page")->with('status', "Предыдущая страница создана");
        }

        return redirect()->route("adm-pages")->with("status", "Страница успешно создана");
    }

    public function edit($id)
    {
        $page = Page::find($id);
        $page->created = str_replace(" ", "T", $page->created_at->format('Y-m-d H:i:s'));

        return view("dashboard.pages.edit-page", compact("page"));
    }

    public function update(Request $request)
    {
//        $request->dd();
        $updated = Page::where("id", $request->input("page_id"))->update([
            "title" => $request->input("title"),
            "slug" => $request->input("slug"),
            "meta_keywords" => $request->input("meta-keywords"),
            "meta_description" => $request->input("meta-description"),
            "short_text" => $request->input("short-text"),
            "text" => $request->input("text"),
            "views" => 1,
            "active" => $request->input("active"),
            "created_at" => $request->input("created_at"),
        ]);

        if(!empty($request->input("create_new"))) {
            return redirect()->route("add-page")->with('status', "Предыдущая страница обновлена");
        }

        return back()->with('status', "Страница обновлена");
    }

    public function remove($id, $url = null)
    {
        Page::where("id", $id)->delete();

        if($url) {
            return redirect()->route("adm-pages")->with("status", "Страница успешно удалена");
        }

        return back()->with("status", "Страница успешно удалена");
    }

    public function duplicate($id)
    {
        $page = Page::where("id", $id)->first();
        $replicated = $page->replicate();
        $replicated->push();

        return redirect()->route("adm-pages")->with("status", "Страница успешно дублирован");
    }

    public function activate($id, int $value)
    {
        Page::where('id', $id)->update([
            "active" => $value
        ]);

        return back()->with("status", "Статус изменен");
    }
}
