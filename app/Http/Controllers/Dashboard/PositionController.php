<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Position;
use Illuminate\Http\Request;

class PositionController extends Controller
{
    public function index()
    {
        $positions = Position::paginate(20);


    }

    public function add(Request $request)
    {
        Position::create([
            "name" => $request->input("name"),
            "active" => $request->input("active") ?? 0,
        ]);
    }

    public function remove($id)
    {
        Position::where("id")->delete();
    }

    public function update(Request $request)
    {
        Position::where('id', $request->input("id"))->update([
            "name" => $request->input("name"),
            "active" => $request->input("active") ?? 0,
        ]);
    }
}
