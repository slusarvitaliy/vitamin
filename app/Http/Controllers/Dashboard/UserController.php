<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Requests\Dashboard\AddUserRequest;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function index()
    {
        $users = User::orderBy('id', 'desc')->paginate(10);

        return view("dashboard.users", compact("users"));
    }

    public function add(AddUserRequest $request)
    {
//        $request->dd();
        User::create([
            'name' => $request->name,
            'email' => $request->email,
            'role' => $request->role,
            'password' => Hash::make($request->password),
        ]);

        return back()->with('status', "Пользователь успешно добавлен");
    }

    public function update(Request $request)
    {
        User::where('id', $request->input("user-id"))->update([
            "name" => $request->input("name"),
            "email" => $request->input("email"),
            "role" => $request->input("role")
        ]);

        if(!empty($request->input("password"))) {
            User::where('id', $request->input("user-id"))->update([
                "password" => Hash::make($request->input("password")),
            ]);
        }

        return back()->with("status", "Пользователь обновлен");
    }

    public function remove($id)
    {
        User::where("id", $id)->delete();

        return back()->with("status", "Пользователь успешно удален");
    }
}
