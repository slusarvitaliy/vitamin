
// global function
function sidebarCollapse() {
    $('#sidebarCollapse').on('click', function() {
        $('#sidebar').toggleClass('active');
        $('#body').toggleClass('active');
    });
}
sidebarCollapse();

function areYouSure() {

    if(document.querySelector(".delete-button")) {
        let deleteButtons = document.querySelectorAll(".delete-button");
        deleteButtons.forEach(element => {
            element.addEventListener("click", () => {
                let link = element.getAttribute("data-url");
                console.log(link);
                document.querySelector(".sure-yes").addEventListener("click", () => {
                    window.location.href = link;
                })
            })
        })
    }

}
areYouSure();

// user page
function userEditListener(users) {

    if(document.querySelector(".edit-button") && window.location.href.includes("adm/users")) {

        let editButtons = document.querySelectorAll(".edit-button");
        editButtons.forEach((element) => {
            element.addEventListener("click", () => {
                let userId = element.getAttribute("data-id");
                let UserData = users.data.filter(e => e.id == userId);
                if(UserData.length != 0) {
                    document.querySelector("#new-name").value = UserData[0].name;
                    document.querySelector("#new-email").value = UserData[0].email;
                    document.querySelector("#new-role").value = UserData[0].role;
                    document.querySelector("#update-user-id").value = UserData[0].id;
                }
            });
        });
    }

};




