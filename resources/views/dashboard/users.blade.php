@extends("dashboard.layout.index")

@section("content-full")
    <div class="col-md-12 col-lg-12">
        <div class="card">
            <div class="card-header">Список пользователей</div>
            <div class="card-body">
                <p class="card-title">
                    <a href="#" class="btn btn-success edit-button"
                       data-toggle="modal" data-target="#userAdd"
                    >
                        <i class="fas fa-user-plus"></i>
                        Добавить нового пользователя
                    </a>
                </p>
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Username</th>
                            <th>E-mail</th>
                            <th>Role</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($users as $user)
                        <tr>
                            <th scope="row">{{$user->id}}</th>
                            <td>
                                {{$user->name}}
                            </td>
                            <td>{{$user->email}}</td>
                            <td>{{$user->role}}</td>
                            <td>
                                <a href="#" class="btn btn-warning edit-button"
                                   data-toggle="modal" data-target="#userEdit"
                                   data-id="{{$user->id}}"
                                >
                                    <i class="fas fa-pencil-alt"></i>
                                </a>
                                <a href="#" class="btn btn-danger delete-button"
                                   data-toggle="modal" data-target="#areYouSure"
                                   data-url="/adm/users/remove/{{$user->id}}"
                                >
                                    <i class="fas fa-trash-alt"></i>
                                </a>
                            </td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>


                </div>
                {{ $users->links() }}
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="userEdit" tabindex="-1" aria-labelledby="userEditLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="userEditLabel">Редактировать пользователя</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form method="post" action="{{route("update-user")}}">
                        @csrf
                        <input type="hidden" id="update-user-id" name="user-id" value="">
                        <div class="form-group">
                            <label for="new-name">Имя пользователя</label>
                            <input type="text" id="new-name" name="name" placeholder="Имя пользователя" class="form-control" required>
                        </div>

                        <div class="form-group">
                            <label for="new-email">E-mail</label>
                            <input type="email" id="new-email" name="email" placeholder="E-mail" class="form-control" required>
                        </div>

                        <div class="form-group">
                            <label for="new-role">Роль</label>
                            <select class="form-control" id="new-role" name="role">
                                <option value="user">Пользователь</option>
                                <option value="admin">Админ</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="password">Пароль</label>
                            <input type="password" id="new-password" name="password" placeholder="Пароль" class="form-control">
                        </div>

                        <div class="form-group">
                            <input type="submit" class="btn btn-primary">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="userAdd" tabindex="-1" aria-labelledby="userAddLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="userEditLabel">Добавить пользователя</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form method="post" action="{{route("add-user")}}">
                        @csrf
                        <div class="form-group">
                            <label for="username">Имя пользователя</label>
                            <input type="text" id="name" name="name" placeholder="Имя пользователя" class="form-control" required>
                        </div>

                        <div class="form-group">
                            <label for="email">E-mail</label>
                            <input type="email" id="email" name="email" placeholder="E-mail" class="form-control" required>
                        </div>

                        <div class="form-group">
                            <label for="role">Роль</label>
                            <select class="form-control" id="role" name="role">
                                <option value="user">Пользователь</option>
                                <option value="admin">Админ</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="password">Пароль</label>
                            <input type="password" id="password" name="password" placeholder="Пароль" class="form-control" required>
                        </div>

                        <div class="form-group">
                            <label for="password_confirmation">Подтвердить Пароль</label>
                            <input type="password" id="password_confirmation" name="password_confirmation" placeholder="Подтвердить Пароль" class="form-control" required>
                        </div>




                        <div class="form-group">
                            <input type="submit" class="btn btn-primary">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>



@endsection

@include("dashboard.partials.modals.are-you-sure-modal")

@section('script')
    <script>
        userEditListener({!! json_encode($users->toArray()) !!});
    </script>
@endsection

