<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Панель управления</title>

    <link href="{{asset("dashboard/vendor/fontawesome/css/fontawesome.min.css")}}" rel="stylesheet">
    <link href="{{asset("dashboard/vendor/fontawesome/css/solid.min.css")}}" rel="stylesheet">
    <link href="{{asset("dashboard/vendor/fontawesome/css/brands.min.css")}}" rel="stylesheet">
    <link href="{{asset("dashboard/css/master.css")}}" rel="stylesheet">
    <link href="{{asset("dashboard/css/app.css")}}" rel="stylesheet">


    @yield("style")
</head>

<body>
<div class="wrapper" id="app">
    @if(Auth::user()->role != "admin")
        <script>window.location = "/";</script>
    @endif

    @include("dashboard.partials.sidebar")

    <div id="body">
        @include("dashboard.partials.navbar")
        <div class="content">
            <div class="container-fluid">
                @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                @endif
                @yield("content-full")
            </div>
            <div class="container">
                @yield("content")
            </div>

        </div>
    </div>

</div>
<script src="{{asset("dashboard/js/app.js")}}"></script>
<script src="{{asset("dashboard/js/main.js")}}"></script>
@yield("script")
</body>

</html>
