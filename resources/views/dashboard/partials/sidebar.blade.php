<nav id="sidebar">
    <div class="sidebar-header">
        <a href="/adm/">
            <img src="{{asset("dashboard/img/vitamin-logo2.png")}}" alt="bootraper logo" class="app-logo">
        </a>

    </div>
    <ul class="list-unstyled components text-secondary">
        <li>
            <a href="#uielementsmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle no-caret-down">
                <i class="fas fa-layer-group"></i> Блоки
            </a>
            <ul class="collapse list-unstyled" id="uielementsmenu">
                <li>
                    <a href="ui-buttons.html"><i class="fas fa-angle-right"></i> Блоки</a>
                </li>
                <li>
                    <a href="ui-badges.html"><i class="fas fa-angle-right"></i> Позиции</a>
                </li>
            </ul>
        </li>
        <li>
            <a href="#pagesmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle no-caret-down">
                <i class="fas fa-file"></i> Страницы
            </a>
            <ul class="collapse list-unstyled" id="pagesmenu">
                <li>
                    <a href="/adm/pages"><i class="fas fa-list-ul"></i> Все Страницы</a>
                </li>
                <li>
                    <a href="/adm/pages/add"><i class="fas fa-plus-square"></i> Добавить Страницу</a>
                </li>
            </ul>
        </li>
        <li>
            <a href="/adm/users"><i class="fas fa-user-friends"></i> Подьзователи</a>
        </li>
        <li>
            <a href="settings.html"><i class="fas fa-cog"></i> Настройки</a>
        </li>
    </ul>
</nav>
