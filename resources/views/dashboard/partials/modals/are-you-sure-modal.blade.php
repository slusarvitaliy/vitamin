<!-- Modal -->
<div class="modal fade" id="areYouSure" tabindex="-1" aria-labelledby="areYouSureLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="areYouSureLabel">Вы уверены?</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-center">
                <button type="button" class="btn btn-danger sure-yes" data-dismiss="modal">Да</button>
                <button type="button" class="btn btn-secondary sure-no" data-dismiss="modal">Нет</button>
            </div>
        </div>
    </div>
</div>
