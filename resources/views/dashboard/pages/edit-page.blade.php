@extends("dashboard.layout.index")

@section("style")
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jodit/3.4.25/jodit.min.css">
@endsection

@section("content-full")
    <form method="post" action="{{route("update-page")}}">
        <input type="hidden" value="{{$page->id}}" name="page_id">
        <input type="hidden" name="back_url" value="{{ url()->previous() }}">
        @csrf
        <div class="row">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        Редактирование страницы
                        <span class="float-right">
                            <input type="submit" class="btn btn-primary" value="Сохранить">
                            <input type="submit" name="create_new" class="btn btn-primary" value="Сохранить и создать">
                            <a href="{{ route("adm-pages") }}" class="btn btn-dark">Закрыть</a>
                            <a href="#" class="btn btn-danger delete-button"
                               data-toggle="modal" data-target="#areYouSure"
                               data-url="/adm/pages/remove/{{$page->id}}/{{"list"}}"
                               title="Удалить"
                            >
                                Удалить
                            </a>
                        </span>

                    </div>
                    <div class="card-body">
                        <div class="form-group">
                            <label for="title">Заглавие</label>
                            <input type="text" name="title" placeholder="Заглавие" class="form-control" required value="{{$page->title}}">
                        </div>

                        <div class="form-group">
                            <label for="short-text">Краткое описание</label>
                            <textarea class="form-control" id="short-text" rows="3" name="short-text">{{$page->short_text}}</textarea>
                        </div>

                        <div class="form-group">
                            <label for="text">Текст страницы</label>
                            <textarea class="form-control" id="text" rows="10" name="text">{{$page->text}}</textarea>
                        </div>

                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card">
                    <div class="card-header">Параметры страницы</div>
                    <div class="card-body">

                        <div class="form-group">
                            <label for="keywords">Ключевые слова</label>
                            <input type="text" name="meta-keywords" placeholder="Ключевые слова через запятую" class="form-control" value="{{$page->meta_keywords}}">
                        </div>

                        <div class="form-group">
                            <label for="meta-description">Meta Description</label>
                            <textarea class="form-control" id="meta-description" rows="3" name="meta-description">{{$page->meta_description}}</textarea>
                        </div>

                        <div class="form-group">
                            <label for="active">Опубликованно</label>
                            <select class="form-control" id="active" name="active">
                                <option value="1" {{$page->active == 1 ? "selected" : ""}}>Да</option>
                                <option value="0" {{$page->active == 0 ? "selected" : ""}}>Нет</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="title">Ссылка</label>
                            <input type="text" name="slug" placeholder="ссылка" class="form-control" required value="{{$page->slug}}">
                        </div>

                        <div class="form-group">
                            <label for="keywords">Дата создания</label>
                            <input type="datetime-local" name="created_at" placeholder="Дата создания" class="form-control" value="{{$page->created}}" >
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection

@section("script")
    <script src="//cdnjs.cloudflare.com/ajax/libs/jodit/3.4.25/jodit.min.js"></script>
    <script>
        let editor = new Jodit('#text');
    </script>
@endsection
@include("dashboard.partials.modals.are-you-sure-modal")

