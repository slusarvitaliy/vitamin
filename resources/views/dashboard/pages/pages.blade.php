@extends("dashboard.layout.index")

@section("content-full")
    <div class="col-md-12 col-lg-12">
        <div class="card">
            <div class="card-header">Список страниц</div>
            <form class="float-right form-inline" action="/adm/pages/filter" method="get">
                <div class="card-body">
                <div class="card-title">
                    <a href="/adm/pages/add" class="btn btn-success">
                        <i class="fas fa-plus-square"></i> Добавить страницу
                    </a>
                    <span class="float-right">
                        <input type="text" class="form-control mb-2 mr-sm-2" id="search" name="search" placeholder="Поиск" value="{{old("search", $search)}}">
                        <button type="submit" class="btn btn-primary mb-2">Искать</button>
                    </span>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>
                                ID
                                <button type="submit" class="btn btn-light" name="id_sort" value="desc"><i class="fas fa-arrow-up"></i></button>
                                <button type="submit" class="btn btn-light" name="id_sort" value="asc"><i class="fas fa-arrow-down"></i></button>
                            </th>
                            <th>
                                Название
                                <button type="submit" class="btn btn-light" name="name_sort" value="desc"><i class="fas fa-arrow-up"></i></button>
                                <button type="submit" class="btn btn-light" name="name_sort" value="asc"><i class="fas fa-arrow-down"></i></button>
                            </th>
                            <th>
                                Активна
                                <button type="submit" class="btn btn-light" name="active_sort" value="desc"><i class="fas fa-arrow-up"></i></button>
                                <button type="submit" class="btn btn-light" name="active_sort" value="asc"><i class="fas fa-arrow-down"></i></button>
                            </th>
                            <th>
                                Создано
                                <button type="submit" class="btn btn-light" name="created_sort" value="desc"><i class="fas fa-arrow-up"></i></button>
                                <button type="submit" class="btn btn-light" name="created_sort" value="asc"><i class="fas fa-arrow-down"></i></button>
                            </th>
                            <th>
                                @if($filtered)
                                    <a href="/adm/pages" class="btn btn-dark" name="created_sort" value="desc">Очистить фильтры</a>
                                @endif
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($pages as $page)
                            <tr>
                                <th scope="row">{{$page->id}}</th>
                                <td>
                                    <a href="/adm/pages/edit/{{$page->id}}">{{$page->title}}</a>
                                </td>
                                <td>
                                    @if($page->active)
                                        <a href="/adm/pages/activate/{{$page->id}}/0" class="btn btn-success"><i class="fas fa-check"></i></a>
                                    @else
                                        <a href="/adm/pages/activate/{{$page->id}}/1" class="btn btn-danger"><i class="fas fa-ban"></i></a>
                                    @endif
                                </td>
                                <td>{{$page->created_at}}</td>
                                <td>
                                    <a href="/adm/pages/duplicate/{{$page->id}}" class="btn btn-warning" title="Дублировать">
                                        <i class="fas fa-copy"></i>
                                    </a>
                                    <a href="#" class="btn btn-danger delete-button"
                                       data-toggle="modal" data-target="#areYouSure"
                                       data-url="/adm/pages/remove/{{$page->id}}"
                                       title="Удалить"
                                    >
                                        <i class="fas fa-trash-alt"></i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>


                </div>
                {{ $pages->links() }}
            </div>
            </form>
        </div>
    </div>
@endsection

@include("dashboard.partials.modals.are-you-sure-modal")
