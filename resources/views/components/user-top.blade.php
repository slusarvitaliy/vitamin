<li class="nav-item dropdown">
    <div class="nav-dropdown">
        <a href="" class="nav-item nav-link dropdown-toggle text-secondary" data-toggle="dropdown">
            <i class="fas fa-user"></i> <span> {{$user->name}}</span>
            <i style="font-size: .8em;" class="fas fa-caret-down"></i>
        </a>
        <div class="dropdown-menu dropdown-menu-right nav-link-menu">
            <ul class="nav-list">
                <li><a href="" class="dropdown-item"><i class="fas fa-address-card"></i> Profile</a></li>
                <li><a href="" class="dropdown-item"><i class="fas fa-envelope"></i> Messages</a></li>
                <li><a href="" class="dropdown-item"><i class="fas fa-cog"></i> Settings</a></li>
                <div class="dropdown-divider"></div>
                <li>
                    <a class="dropdown-item" href="{{ route('logout') }}"
                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                        <i class="fas fa-sign-out-alt"></i> {{ __('Logout') }}
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                        @csrf
                    </form>
                </li>
            </ul>
        </div>
    </div>
</li>
